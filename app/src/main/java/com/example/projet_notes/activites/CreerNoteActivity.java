package com.example.projet_notes.activites;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projet_notes.R;
import com.example.projet_notes.basedonnees.NotesBD;
import com.example.projet_notes.entities.Note;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CreerNoteActivity extends AppCompatActivity {

    private EditText inputTitreNote, inputSousTitreNote, inputContenuNote;
    private TextView textDate;
    private View viewSousTitreCouleur;
    private ImageView noteImage;
    private TextView texteWebUrl;
    private LinearLayout layoutWebUrl;

    private String selectedNoteColor;
    private String selectionCheminImage;

    private static final int CODE_REQUETE_PERMISSION = 1;
    private static final int CODE_REQUETE_SELECTION_IMAGE = 2;

    private AlertDialog dialogueAjouterUrl;
    private AlertDialog dialogueSupprimerNote;

    private Note dejaDisponibleNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creer_note);

        ImageView image_retour = findViewById(R.id.image_retour);
        image_retour.setOnClickListener(v -> onBackPressed());

        inputTitreNote = findViewById(R.id.inputNoteTitre);
        inputSousTitreNote = findViewById(R.id.inputNoteSousTitre);
        inputContenuNote = findViewById(R.id.inputNoteContenu);
        textDate = findViewById(R.id.texteDate);
        viewSousTitreCouleur=findViewById(R.id.viewSousTitreCouleur);
        noteImage = findViewById(R.id.imageNote);
        texteWebUrl = findViewById(R.id.texteWebUrl);
        layoutWebUrl = findViewById(R.id.layoutWebUrl);

        textDate.setText(new SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm", Locale.getDefault()).format(new Date()));

        ImageView imageSauvegarder = findViewById(R.id.imageSauvegarder);
        imageSauvegarder.setOnClickListener(v -> saveNote());
        selectedNoteColor="#333333";
        selectionCheminImage="";

        if(getIntent().getBooleanExtra("voir_ou_maj", false)){
            dejaDisponibleNote = (Note) getIntent().getSerializableExtra("note");
            setVoirOuUpdateNote();
        }

        findViewById(R.id.imageSuppressionWebUrl).setOnClickListener(v -> {
            texteWebUrl.setText(null);
            layoutWebUrl.setVisibility(View.GONE);

        });

        findViewById(R.id.imageSuppressionImage).setOnClickListener(v -> {
            noteImage.setImageBitmap(null);
            noteImage.setVisibility(View.GONE);
            findViewById(R.id.imageSuppressionImage).setVisibility(View.GONE);
            selectionCheminImage ="";
        });

        if(getIntent().getBooleanExtra("isFrontQuickActions",false)){
            String type = getIntent().getStringExtra("QuickActionType");
            if(type !=null){
                if(type.equals("image")){
                    selectionCheminImage = getIntent().getStringExtra("imagePath");
                    noteImage.setImageBitmap(BitmapFactory.decodeFile(selectionCheminImage));
                    noteImage.setVisibility(View.VISIBLE);
                    findViewById(R.id.imageSuppressionImage).setVisibility(View.VISIBLE);

                }else if(type.equals("URL")){
                    texteWebUrl.setText(getIntent().getStringExtra("URL"));
                    layoutWebUrl.setVisibility((View.VISIBLE));
                }

            }

        }

        initDivers();
        setSubtitleIndicatorColor();
    }

    private void setVoirOuUpdateNote(){
        inputTitreNote.setText(dejaDisponibleNote.getTitre());
        inputSousTitreNote.setText(dejaDisponibleNote.getSousTitres());
        inputContenuNote.setText(dejaDisponibleNote.getNotesTexte());
        textDate.setText(dejaDisponibleNote.getDateTime());

        if(dejaDisponibleNote.getImgpath() != null && !dejaDisponibleNote.getImgpath().trim().isEmpty()){
            noteImage.setImageBitmap(BitmapFactory.decodeFile(dejaDisponibleNote.getImgpath()));
            noteImage.setVisibility(View.VISIBLE);
            findViewById(R.id.imageSuppressionImage).setVisibility(View.VISIBLE);
            selectionCheminImage = dejaDisponibleNote.getImgpath();
        }

        if(dejaDisponibleNote.getLienWeb() != null && !dejaDisponibleNote.getLienWeb().trim().isEmpty()){
            texteWebUrl.setText(dejaDisponibleNote.getLienWeb());
            layoutWebUrl.setVisibility(View.VISIBLE);
        }

    }

    private void saveNote(){
        if(inputTitreNote.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Le titre de la note ne peut pas être vide", Toast.LENGTH_SHORT).show();
            return;
        }else if (inputSousTitreNote.getText().toString().trim().isEmpty() && inputContenuNote.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "La note ne peut pas être vide", Toast.LENGTH_SHORT).show();
            return;
        }

        final Note note = new Note();
        note.setTitre(inputTitreNote.getText().toString());
        note.setSousTitres(inputSousTitreNote.getText().toString());
        note.setNotesTexte(inputContenuNote.getText().toString());
        note.setDateTime(textDate.getText().toString());
        note.setCouleurs(selectedNoteColor);
        note.setImgpath(selectionCheminImage);

        //si il est visible c'est qu'on a ajouté un url
        if (layoutWebUrl.getVisibility() == View.VISIBLE){
            note.setLienWeb(texteWebUrl.getText().toString());
        }

        if(dejaDisponibleNote != null){
            note.setId(dejaDisponibleNote.getId()); //pour remplacer la note dans la BD
        }                             // car dans NoteDao --> onConflictStrategy est set à REPLACE

        //Async --> effectuer en tâche de fond
        @SuppressLint("StaticFieldLeak")
        class SauvegarderNoteTask extends AsyncTask<Void, Void, Void>{

            @Override
            protected Void doInBackground(Void... voids){
                NotesBD.getNotesBD(getApplicationContext()).noteDao().insertNote(note);
                return null;
            }

            @Override
            protected  void onPostExecute(Void unVoid){
                super.onPostExecute(unVoid);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        }
        new SauvegarderNoteTask().execute();
    }

    private void initDivers(){
        final LinearLayout layoutDivers = findViewById(R.id.layoutDivers);
        final BottomSheetBehavior<LinearLayout> bottomSheetBehavior = BottomSheetBehavior.from(layoutDivers);
        layoutDivers.findViewById(R.id.textDivers).setOnClickListener(v -> {
            if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED){
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
        final ImageView imageColor1 = layoutDivers.findViewById(R.id.imageColor1);
        final ImageView imageColor2 = layoutDivers.findViewById(R.id.imageColor2);
        final ImageView imageColor3 = layoutDivers.findViewById(R.id.imageColor3);
        final ImageView imageColor4 = layoutDivers.findViewById(R.id.imageColor4);
        final ImageView imageColor5 = layoutDivers.findViewById(R.id.imageColor5);

        layoutDivers.findViewById(R.id.viewColor1).setOnClickListener(v -> {
            selectedNoteColor = "#222222";
            imageColor1.setImageResource(R.drawable.icone_fini);
            imageColor2.setImageResource(0);
            imageColor3.setImageResource(0);
            imageColor4.setImageResource(0);
            imageColor5.setImageResource(0);
            setSubtitleIndicatorColor();
        });

        layoutDivers.findViewById(R.id.viewColor2).setOnClickListener(v -> {
            selectedNoteColor = "#F2E9CD";
            imageColor1.setImageResource(0);
            imageColor2.setImageResource(R.drawable.icone_fini);
            imageColor3.setImageResource(0);
            imageColor4.setImageResource(0);
            imageColor5.setImageResource(0);
            setSubtitleIndicatorColor();
        });
        layoutDivers.findViewById(R.id.viewColor3).setOnClickListener(v -> {
            selectedNoteColor = "#81106B";
            imageColor1.setImageResource(0);
            imageColor2.setImageResource(0);
            imageColor3.setImageResource(R.drawable.icone_fini);
            imageColor4.setImageResource(0);
            imageColor5.setImageResource(0);
            setSubtitleIndicatorColor();
        });
        layoutDivers.findViewById(R.id.viewColor4).setOnClickListener(v -> {
            selectedNoteColor = "#016972";
            imageColor1.setImageResource(0);
            imageColor2.setImageResource(0);
            imageColor3.setImageResource(0);
            imageColor4.setImageResource(R.drawable.icone_fini);
            imageColor5.setImageResource(0);
            setSubtitleIndicatorColor();
        });
        layoutDivers.findViewById(R.id.viewColor5).setOnClickListener(v -> {
            selectedNoteColor = "#FF6666";
            imageColor1.setImageResource(0);
            imageColor2.setImageResource(0);
            imageColor3.setImageResource(0);
            imageColor4.setImageResource(0);
            imageColor5.setImageResource(R.drawable.icone_fini);
            setSubtitleIndicatorColor();
        });

        if(dejaDisponibleNote != null && dejaDisponibleNote.getCouleurs() != null && !dejaDisponibleNote.getCouleurs().trim().isEmpty()){
            switch (dejaDisponibleNote.getCouleurs()){
                case "#F2E9CD":
                    layoutDivers.findViewById(R.id.viewColor2).performClick();
                    break;
                case "#81106B":
                    layoutDivers.findViewById(R.id.viewColor3).performClick();
                    break;
                case "#016972":
                    layoutDivers.findViewById(R.id.viewColor4).performClick();
                    break;
                case "#FF6666":
                    layoutDivers.findViewById(R.id.viewColor5).performClick();
                    break;
            }
        }

        layoutDivers.findViewById(R.id.layoutAjouterImage).setOnClickListener(v -> {
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        if (ContextCompat.checkSelfPermission(
                getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE
                )!= PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(
                        CreerNoteActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        CODE_REQUETE_PERMISSION
            );
        }else{
            selectionImage();
        }
        });

        layoutDivers.findViewById(R.id.layoutAjouterUrl).setOnClickListener(v -> {
             bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
             showAjouterUrlDialogue();
        });

        if(dejaDisponibleNote != null){
            layoutDivers.findViewById(R.id.layoutSupprimerNote).setVisibility(View.VISIBLE);
            layoutDivers.findViewById(R.id.layoutSupprimerNote).setOnClickListener(v -> {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                showSupprimerNoteDialogue();
            });
        }

    }

    private void showSupprimerNoteDialogue(){
        if(dialogueSupprimerNote == null){
            AlertDialog.Builder builder = new AlertDialog.Builder(CreerNoteActivity.this);
            View view = LayoutInflater.from(this).inflate(
                    R.layout.layout_supprimer_note,
                    findViewById(R.id.layoutSupprimerNoteContainer)
            );
            builder.setView(view);
            dialogueSupprimerNote = builder.create();
            if(dialogueSupprimerNote.getWindow() != null){
                dialogueSupprimerNote.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            }
            view.findViewById(R.id.texteSupprimerNote).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    @SuppressLint("StaticFieldLeak")
                    class TacheSupprimerNote extends AsyncTask<Void, Void, Void>{

                        @Override
                        protected Void doInBackground(Void... voids) {
                            NotesBD.getNotesBD(getApplicationContext()).noteDao()
                                    .deleteNote(dejaDisponibleNote);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            Intent intent = new Intent();
                            intent.putExtra("noteSupprimee", true);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }

                    new TacheSupprimerNote().execute();
                }
            });

            view.findViewById(R.id.texteAnnuler).setOnClickListener(v -> dialogueSupprimerNote.dismiss());
        }

        dialogueSupprimerNote.show();
    }

    private void setSubtitleIndicatorColor(){
        GradientDrawable gradientDrawable=(GradientDrawable) viewSousTitreCouleur.getBackground();
        gradientDrawable.setColor(Color.parseColor(selectedNoteColor));

    }

    private void selectionImage(){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (intent.resolveActivity((getPackageManager()))!=null){
            startActivityForResult(intent,CODE_REQUETE_SELECTION_IMAGE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode==CODE_REQUETE_PERMISSION && grantResults.length > 0){
            if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
                selectionImage();
            }else {
                Toast.makeText(this, "Refus de permission", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode== CODE_REQUETE_SELECTION_IMAGE && resultCode == RESULT_OK){
            if(data != null){
                Uri selectedImageUri = data.getData();
                if(selectedImageUri != null){
                    try{
                        InputStream inputStream = getContentResolver().openInputStream(selectedImageUri);
                        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                        noteImage.setImageBitmap(bitmap);
                        noteImage.setVisibility(View.VISIBLE);
                        findViewById(R.id.imageSuppressionImage).setVisibility(View.VISIBLE);

                        selectionCheminImage = getPathFromUri(selectedImageUri);

                    }catch (Exception exception){
                        Toast.makeText(this, exception.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }
    private String getPathFromUri (Uri contenuUri){
        String cheminFic;
        Cursor cursor  = getContentResolver()
                .query(contenuUri,null,null,null,null);
        if(cursor==null){
            cheminFic = contenuUri.getPath();
        }else{
            cursor.moveToFirst();
            int index = cursor.getColumnIndex("_data");
            cheminFic = cursor.getString(index);
            cursor.close();
        }
        return cheminFic;

    }

    private void showAjouterUrlDialogue(){
        if(dialogueAjouterUrl == null){
            AlertDialog.Builder builder = new AlertDialog.Builder(CreerNoteActivity.this);
            View view = LayoutInflater.from(this).inflate(
                    R.layout.layout_ajouter_url,
                    findViewById(R.id.layoutAjouterUrlContainer)
            );
            builder.setView(view);

            dialogueAjouterUrl = builder.create();
            if(dialogueAjouterUrl.getWindow() != null){
                dialogueAjouterUrl.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            }
            final EditText inputUrl = view.findViewById(R.id.inputURL);
            inputUrl.requestFocus();

            view.findViewById(R.id.ajouterTexte).setOnClickListener(v -> {
                if(inputUrl.getText().toString().trim().isEmpty()){
                    Toast.makeText(CreerNoteActivity.this, "Entrez une URL", Toast.LENGTH_SHORT).show();
                }else if (!Patterns.WEB_URL.matcher(inputUrl.getText().toString()).matches()){
                    Toast.makeText(CreerNoteActivity.this, "Entrez une URL valide", Toast.LENGTH_SHORT).show();
                }else{
                    texteWebUrl.setText(inputUrl.getText().toString());
                    layoutWebUrl.setVisibility(View.VISIBLE);
                    dialogueAjouterUrl.dismiss();
                }
            });

            view.findViewById(R.id.annulerTexte).setOnClickListener(v -> dialogueAjouterUrl.dismiss());
        }
        dialogueAjouterUrl.show();
    }
}