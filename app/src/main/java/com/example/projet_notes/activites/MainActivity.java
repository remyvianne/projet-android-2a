package com.example.projet_notes.activites;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.projet_notes.R;
import com.example.projet_notes.adapters.NotesAdapters;
import com.example.projet_notes.basedonnees.NotesBD;
import com.example.projet_notes.entities.Note;
import com.example.projet_notes.listeners.NotesListener;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NotesListener {

    public static final int REQUEST_CODE_AJOUTER_NOTE = 1; // ajouter une note
    public static final int REQUEST_CODE_MAJ_NOTE = 2; // mettre à jours les notes
    public static final int REQUEST_CODE_VOIR_NOTES = 3; // affichage des notes
    public static final int REQUEST_CODE_SELECTION_IMAGE =4;
    public static final int REQUEST_CODE_STOCK_PERMS = 5;

    private RecyclerView notesRecyclerView;
    private List<Note> notesListe;
    private NotesAdapters notesAdapters;

    private int noteClickedPosition = -1;

    private AlertDialog dialogueAjouterUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imageAjouterNoteMain = findViewById(R.id.imageAjouterNote);
        imageAjouterNoteMain.setOnClickListener(v -> startActivityForResult(
                new Intent(getApplicationContext(), CreerNoteActivity.class),
                REQUEST_CODE_AJOUTER_NOTE
        ));

        notesRecyclerView = findViewById(R.id.notesRecyclerView);
        notesRecyclerView.setLayoutManager(
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        );

        notesListe = new ArrayList<>();
        notesAdapters = new NotesAdapters(notesListe, this);
        notesRecyclerView.setAdapter(notesAdapters);

        getNotes(REQUEST_CODE_VOIR_NOTES, false);

        EditText inputSearch = findViewById(R.id.inputSearch);
        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                notesAdapters.annulerTimer();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (notesListe.size() != 0) {
                    notesAdapters.rechercheNotes(s.toString());
                }
            }
        });
        findViewById(R.id.imageAjouterImage).setOnClickListener(v -> {
            if (ContextCompat.checkSelfPermission(
                    getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE
            )!= PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(
                        MainActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE_STOCK_PERMS
                );
            }else{
                selectionImage();
            }

        });

        findViewById(R.id.imageLienWeb).setOnClickListener(v -> showAjouterUrlDialogue());
    }

    private void selectionImage(){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (intent.resolveActivity((getPackageManager()))!=null){
            startActivityForResult(intent,REQUEST_CODE_SELECTION_IMAGE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode==REQUEST_CODE_STOCK_PERMS && grantResults.length > 0){
            if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
                selectionImage();
            }else {
                Toast.makeText(this, "Refus de permission", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private String getPathFromUri (Uri contenuUri){
        String cheminFic;
        Cursor cursor  = getContentResolver()
                .query(contenuUri,null,null,null,null);
        if(cursor==null){
            cheminFic = contenuUri.getPath();
        }else{
            cursor.moveToFirst();
            int index = cursor.getColumnIndex("_data");
            cheminFic = cursor.getString(index);
            cursor.close();
        }
        return cheminFic;

    }

    @Override
    public void onNoteClicked(Note note, int position) {
        noteClickedPosition = position;
        Intent intent = new Intent(getApplicationContext(), CreerNoteActivity.class);
        intent.putExtra("voir_ou_maj", true);
        intent.putExtra("note", note);
        startActivityForResult(intent, REQUEST_CODE_MAJ_NOTE);
    }

    private void getNotes(final int requestCode, final boolean noteSupprimee) {
        //AsyncTask --> pour récuperer les notes dans la BD
        class GetNotesTask extends AsyncTask<Void, Void, List<Note>> {

            @Override
            protected List<Note> doInBackground(Void... voids) {
                return NotesBD.getNotesBD(getApplicationContext()).noteDao().getAllNotes();
            }

            @Override
            protected void onPostExecute(List<Note> notes) {
                super.onPostExecute(notes);
                if (requestCode == REQUEST_CODE_VOIR_NOTES) {
                    notesListe.addAll(notes);
                    notesAdapters.notifyDataSetChanged();
                } else if (requestCode == REQUEST_CODE_AJOUTER_NOTE) {
                    notesListe.add(0, notes.get(0));
                    notesAdapters.notifyItemInserted(0);
                    notesRecyclerView.smoothScrollToPosition(0); //scroll jusqu'en haut du recycler
                } else if (requestCode == REQUEST_CODE_MAJ_NOTE) {
                    notesListe.remove(noteClickedPosition);
                    if (noteSupprimee) {
                        notesAdapters.notifyItemRemoved(noteClickedPosition);
                    } else {
                        notesListe.add(noteClickedPosition, notes.get(noteClickedPosition));
                        notesAdapters.notifyItemChanged(noteClickedPosition);
                    }
                }
            }
        }
        new GetNotesTask().execute();
    }

    //Pour afficher la derniere notes que l'on ajoute
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_AJOUTER_NOTE && resultCode == RESULT_OK) {
            getNotes(REQUEST_CODE_AJOUTER_NOTE, false);
        } else if (requestCode == REQUEST_CODE_MAJ_NOTE && resultCode == RESULT_OK) {
            if (data != null) {
                getNotes(REQUEST_CODE_MAJ_NOTE, data.getBooleanExtra("noteSupprimee", false));
            }
        }else if(requestCode==REQUEST_CODE_SELECTION_IMAGE && resultCode == RESULT_OK){
            if(data !=null){
                Uri selectionImageUri = data.getData();
                if(selectionImageUri != null){
                    try {
                        String selectedImagePath = getPathFromUri(selectionImageUri);
                        Intent intent = new Intent(getApplicationContext(),CreerNoteActivity.class);
                        intent.putExtra("isFrontQuickActions",true);
                        intent.putExtra("QuickActionType","image");
                        intent.putExtra("imagePath",selectedImagePath);
                        startActivityForResult(intent,REQUEST_CODE_AJOUTER_NOTE);

                    }catch(Exception exception){
                        Toast.makeText(this, exception.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

        }
    }

    private void showAjouterUrlDialogue(){
        if(dialogueAjouterUrl == null){
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            View view = LayoutInflater.from(this).inflate(
                    R.layout.layout_ajouter_url,
                    findViewById(R.id.layoutAjouterUrlContainer)
            );
            builder.setView(view);

            dialogueAjouterUrl = builder.create();
            if(dialogueAjouterUrl.getWindow() != null){
                dialogueAjouterUrl.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            }
            final EditText inputUrl = view.findViewById(R.id.inputURL);
            inputUrl.requestFocus();

            view.findViewById(R.id.ajouterTexte).setOnClickListener(v -> {
                if(inputUrl.getText().toString().trim().isEmpty()){
                    Toast.makeText(MainActivity.this, "Entrez une URL", Toast.LENGTH_SHORT).show();
                }else if (!Patterns.WEB_URL.matcher(inputUrl.getText().toString()).matches()){
                    Toast.makeText(MainActivity.this, "Entrez une URL valide", Toast.LENGTH_SHORT).show();
                }else{
                    dialogueAjouterUrl.dismiss();
                    Intent intent = new Intent(getApplicationContext(),CreerNoteActivity.class);
                    intent.putExtra("isFrontQuickActions",true);
                    intent.putExtra("QuickActionType","URL");
                    intent.putExtra("URL",inputUrl.getText().toString());
                    startActivityForResult(intent,REQUEST_CODE_AJOUTER_NOTE);
                }
            });

            view.findViewById(R.id.annulerTexte).setOnClickListener(v -> dialogueAjouterUrl.dismiss());
        }
        dialogueAjouterUrl.show();
    }
}