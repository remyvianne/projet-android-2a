package com.example.projet_notes.adapters;

import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projet_notes.R;
import com.example.projet_notes.entities.Note;
import com.example.projet_notes.listeners.NotesListener;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class NotesAdapters extends RecyclerView.Adapter<NotesAdapters.NoteViewHolder> {

    private List<Note> notes;
    private NotesListener notesListener;
    private Timer timer;
    private List<Note> notesSource;

    public NotesAdapters(List<Note> notes, NotesListener notesListener) {

        this.notes = notes;
        this.notesListener = notesListener;
        notesSource = notes;
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NoteViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_container_note,
                        parent,
                        false
                )
        );
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder holder, int position) {
        holder.setNote(notes.get(position));
        holder.layoutNote.setOnClickListener(v -> notesListener.onNoteClicked(notes.get(position), position));
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    static class NoteViewHolder extends RecyclerView.ViewHolder {

        TextView texteTitre, texteSousTitre, texteDate;
        LinearLayout layoutNote;
        RoundedImageView imageNote;


        public NoteViewHolder(@NonNull View itemView) {
            super(itemView);
            texteTitre = itemView.findViewById(R.id.texteTitre);
            texteSousTitre = itemView.findViewById(R.id.texteSousTitre);
            texteDate = itemView.findViewById(R.id.texteDate);
            layoutNote = itemView.findViewById(R.id.layoutNote);
            imageNote = itemView.findViewById(R.id.imageNote);
        }

        void setNote(Note note) {
            texteTitre.setText(note.getTitre());
            if (note.getSousTitres().trim().isEmpty()) {
                texteSousTitre.setVisibility((View.GONE));
            } else {
                texteSousTitre.setText(note.getSousTitres());
            }
            texteDate.setText(note.getDateTime());

            GradientDrawable gradientDrawable = (GradientDrawable) layoutNote.getBackground();
            if (note.getCouleurs() != null) {
                gradientDrawable.setColor(Color.parseColor(note.getCouleurs()));
            } else {
                gradientDrawable.setColor(Color.parseColor("#333333"));
            }
            if (note.getImgpath() != null) {
                imageNote.setImageBitmap(BitmapFactory.decodeFile(note.getImgpath()));
                imageNote.setVisibility(View.VISIBLE);
            } else {
                imageNote.setVisibility(View.GONE);
            }
        }
    }

    public void rechercheNotes(final String rechercheMotsCle) {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (rechercheMotsCle.trim().isEmpty()) {
                    notes = notesSource;
                } else {
                    ArrayList<Note> temp = new ArrayList<>();
                    for (Note note : notesSource) {
                        if (note.getTitre().toLowerCase().contains(rechercheMotsCle.toLowerCase())
                                || note.getSousTitres().toLowerCase().contains(rechercheMotsCle.toLowerCase())
                                || note.getNotesTexte().toLowerCase().contains(rechercheMotsCle.toLowerCase())) {
                            temp.add(note);
                        }
                    }
                    notes = temp;
                }
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                    }
                });
            }
        }, 500);
    }
    public void annulerTimer(){
        if (timer != null){
            timer.cancel();
        }
    }
}
