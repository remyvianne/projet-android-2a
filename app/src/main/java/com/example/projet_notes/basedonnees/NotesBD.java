package com.example.projet_notes.basedonnees;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.projet_notes.dao.NoteDao;
import com.example.projet_notes.entities.Note;

@Database(entities = Note.class,version = 1,exportSchema = false)
public abstract class NotesBD extends RoomDatabase {

    private static NotesBD notesBD;

    public static synchronized NotesBD getNotesBD(Context context){
        if (notesBD == null){
            notesBD = Room.databaseBuilder(
                    context,
                    NotesBD.class,
                    "notes_bd"
            ).build();
        }
        return notesBD;
    }

    public abstract NoteDao noteDao();


}

