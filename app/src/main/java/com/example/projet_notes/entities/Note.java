package com.example.projet_notes.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;


@Entity(tableName = "notes")
public class Note implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name= "titre")
    private String titre;

    @ColumnInfo(name= "date_t")
    private String dateTime;

    @ColumnInfo(name= "sous_titres")
    private String sousTitres;

    @ColumnInfo(name= "texte_notes")
    private String notesTexte;

    @ColumnInfo(name= "image")
    private String imgpath;

    @ColumnInfo(name= "couleurs")
    private String couleurs;

    @ColumnInfo(name= "lien_web")
    private String lienWeb;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getSousTitres() {
        return sousTitres;
    }

    public void setSousTitres(String sousTitres) {
        this.sousTitres = sousTitres;
    }

    public String getNotesTexte() {
        return notesTexte;
    }

    public void setNotesTexte(String notesTexte) {
        this.notesTexte = notesTexte;
    }

    public String getImgpath() {
        return imgpath;
    }

    public void setImgpath(String imgpath) {
        this.imgpath = imgpath;
    }

    public String getCouleurs() {
        return couleurs;
    }

    public void setCouleurs(String couleurs) {
        this.couleurs = couleurs;
    }

    public String getLienWeb() {
        return lienWeb;
    }

    public void setLienWeb(String lienWeb) {
        this.lienWeb = lienWeb;
    }

    @NonNull
    @Override
    public String toString() {
        return titre + ": " + dateTime;
    }
}
