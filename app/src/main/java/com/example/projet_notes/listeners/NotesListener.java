package com.example.projet_notes.listeners;

import com.example.projet_notes.entities.Note;

public interface NotesListener {
    void onNoteClicked(Note note, int position);
}
